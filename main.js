function toggleSubmit() {
  $(".showInputValue").text(answerSlider + " out of 100 Estonians");
}

function toggleAnswer() {
  $('.main-submit').fadeOut(400);
  $('.main-helper').fadeOut(400);
  $('.slider').fadeOut(400);

  theAnswerIs(rightAnswer);

  if (Math.abs(answerSlider - rightAnswer) <= 5) {
    $(".main-right").css("display", "block");
    $(".main-right").text("You guessed " + answerSlider + ". Well done! The right answer is " + rightAnswer + ".");
    //add some points
  } else {
    $(".main-wrong").css("display", "block");
    $(".main-wrong").text("You guessed " + answerSlider + ". Not quite. The right answer is " + rightAnswer + ".");
    //add no points
  }
  $(".main-average").css("display", "block");
  $(".main-average").text("On average, people think it's " + average + ".");
  //do we calculate a score for the bias?

  $(".main-next").css("display", "block");

  $.ajax({
    url: "answer/",
    type:'POST',
    data: JSON.stringify({
      questionID: curQuestion.toString(),
      answer: answerSlider
    }),
    contentType: "application/json",
    crossDomain:true,
    crossOrigin: true,
    xhrFields: { withCredentials: true },
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    success: function(data){
    }
  }).then( function() {
  });
}

let questionContent = "";
let rightAnswer = "";
let allReplies = [];
let average = 0;
let curQuestion = 1;

function nextQuestion() {
  if(parseInt(curQuestion, 10) > 8) {  //test this please. (curQuestion == 9) does work but not ideal
    curQuestion = 0;
    window.location.href = "/results.html";
  } else {
    location.reload();
  }
  localStorage.setItem('lastAnswer', parseInt(curQuestion, 10) + 1);
}

let lastAnswerCheck = (localStorage.getItem('lastAnswer'));
if(lastAnswerCheck == null) {
  curQuestion = 1;
  localStorage.setItem('lastAnswer', parseInt(curQuestion, 10));
} else {
  curQuestion = localStorage.getItem('lastAnswer');
}

function calcAverage(elmt){
  var sum = 0;
  for( var i = 0; i < elmt.length; i++ ){
      sum += parseInt( elmt[i], 10 ); //don't forget to add the base
  }

  var avg = sum/elmt.length;
  return Math.round(avg);
}

$( document ).ready(function() {
  $(".main-submit").text("Submit");
  $("#slider").val(0);

  $.ajax({
    url: "question/"+curQuestion.toString(),
    type:'GET',
    contentType: "html",
    dataType: "jsonp",
    jsonpCallback: 'callback',
    crossDomain:true,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    success: function(data){
      questionContent = data.res.question;
      rightAnswer = data.res.answer;
      allReplies = data.res.replies;
      return true;
    }
  }).then( function() {
    average = calcAverage(allReplies);
    $(".main-title").text(questionContent);
  });

});
