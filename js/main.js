let answerSlider = 0;
let canvasBab = document.getElementsByClassName("main-canvas")[0];
let engine = new BABYLON.Engine(canvasBab, true);
let hereItIs = 0;
let answerIsAcquired = false;

var createScene = function () {
    let scene = new BABYLON.Scene(engine);
    canvasBab = engine.getRenderingCanvas();

    shineColorMaterial =  new BABYLON.StandardMaterial("selectedMaterial", scene);
    shineColorMaterial.diffuseColor=new BABYLON.Color4.FromHexString("#22222200");

    personTextureMat = shineColorMaterial.clone();
    personTextureMat.diffuseColor = new BABYLON.Color4.FromHexString("#3571ffff");

    defaultValuePerson = shineColorMaterial.clone();
    defaultValuePerson.diffuseColor = new BABYLON.Color4.FromHexString("#747577ff");

    correctValuePerson = shineColorMaterial.clone();
    correctValuePerson.emissiveColor = new BABYLON.Color4.FromHexString("#278427ff");

    wrongValuePerson = shineColorMaterial.clone();
    wrongValuePerson.diffuseColor = new BABYLON.Color4.FromHexString("#ff6750ff");

    cameraUni = new BABYLON.ArcRotateCamera("cam", 0, Math.PI / 2, 0, new BABYLON.Vector3(0, -1, 0), scene); //these change the centre camera not angle
    light = new BABYLON.HemisphericLight("sun", new BABYLON.Vector3(0.25, 0.75, 0), scene);
    cameraUni.attachControl(canvasBab, true);
    cameraUni.setPosition(new BABYLON.Vector3(x = 0, y = 9, z = -8.282483814634071)); //alpha,beta,rad
    cameraUni.lowerRadiusLimit = 22.9; //closest
    cameraUni.upperRadiusLimit = 22.9; //furthest
    cameraUni.inertia = 0.97;
    cameraUni.useAutoRotationBehavior = true;
    cameraUni.autoRotationBehavior.idleRotationSpeed = 0.02;
    cameraUni.autoRotationBehavior.idleRotationWaitTime = 100000;
    cameraUni.lowerBetaLimit = 0.7;
    cameraUni.upperBetaLimit = 1.3;
    cameraUni.useFramingBehavior = true;

    var gl = new BABYLON.GlowLayer("glow", scene);

    BABYLON.SceneLoader.ImportMesh("", "js/", "1003Dbabylon.js", scene, function (scene) {
            getAllSubMeshes();
            for (i = 0; i < 100; i++) {
                person2Array[i].material = defaultValuePerson;
            }
            for (i = 0; i < 100; i++) {
                //personArray[i].isVisible = -10000;
                person2Array[i].position.y = 0.5;
            }
    });


    function getAllSubMeshes() {

        person2Array = [

            scene.getMeshByName("Plan.001"),
            scene.getMeshByName("Plan.002"),
            scene.getMeshByName("Plan.003"),
            scene.getMeshByName("Plan.004"),
            scene.getMeshByName("Plan.005"),
            scene.getMeshByName("Plan.006"),
            scene.getMeshByName("Plan.007"),
            scene.getMeshByName("Plan.008"),
            scene.getMeshByName("Plan.009"),
            scene.getMeshByName("Plan.010"),

            scene.getMeshByName("Plan.011"),
            scene.getMeshByName("Plan.012"),
            scene.getMeshByName("Plan.013"),
            scene.getMeshByName("Plan.014"),
            scene.getMeshByName("Plan.015"),
            scene.getMeshByName("Plan.016"),
            scene.getMeshByName("Plan.017"),
            scene.getMeshByName("Plan.018"),
            scene.getMeshByName("Plan.019"),
            scene.getMeshByName("Plan.020"),

            scene.getMeshByName("Plan.021"),
            scene.getMeshByName("Plan.022"),
            scene.getMeshByName("Plan.023"),
            scene.getMeshByName("Plan.024"),
            scene.getMeshByName("Plan.025"),
            scene.getMeshByName("Plan.026"),
            scene.getMeshByName("Plan.027"),
            scene.getMeshByName("Plan.028"),
            scene.getMeshByName("Plan.029"),
            scene.getMeshByName("Plan.030"),

            scene.getMeshByName("Plan.031"),
            scene.getMeshByName("Plan.032"),
            scene.getMeshByName("Plan.033"),
            scene.getMeshByName("Plan.034"),
            scene.getMeshByName("Plan.035"),
            scene.getMeshByName("Plan.036"),
            scene.getMeshByName("Plan.037"),
            scene.getMeshByName("Plan.038"),
            scene.getMeshByName("Plan.039"),
            scene.getMeshByName("Plan.040"),

            scene.getMeshByName("Plan.041"),
            scene.getMeshByName("Plan.042"),
            scene.getMeshByName("Plan.043"),
            scene.getMeshByName("Plan.044"),
            scene.getMeshByName("Plan.045"),
            scene.getMeshByName("Plan.046"),
            scene.getMeshByName("Plan.047"),
            scene.getMeshByName("Plan.048"),
            scene.getMeshByName("Plan.049"),
            scene.getMeshByName("Plan.050"),

            scene.getMeshByName("Plan.051"),
            scene.getMeshByName("Plan.052"),
            scene.getMeshByName("Plan.053"),
            scene.getMeshByName("Plan.054"),
            scene.getMeshByName("Plan.055"),
            scene.getMeshByName("Plan.056"),
            scene.getMeshByName("Plan.057"),
            scene.getMeshByName("Plan.058"),
            scene.getMeshByName("Plan.059"),
            scene.getMeshByName("Plan.060"),

            scene.getMeshByName("Plan.061"),
            scene.getMeshByName("Plan.062"),
            scene.getMeshByName("Plan.063"),
            scene.getMeshByName("Plan.064"),
            scene.getMeshByName("Plan.065"),
            scene.getMeshByName("Plan.066"),
            scene.getMeshByName("Plan.067"),
            scene.getMeshByName("Plan.068"),
            scene.getMeshByName("Plan.069"),
            scene.getMeshByName("Plan.070"),

            scene.getMeshByName("Plan.071"),
            scene.getMeshByName("Plan.072"),
            scene.getMeshByName("Plan.073"),
            scene.getMeshByName("Plan.074"),
            scene.getMeshByName("Plan.075"),
            scene.getMeshByName("Plan.076"),
            scene.getMeshByName("Plan.077"),
            scene.getMeshByName("Plan.078"),
            scene.getMeshByName("Plan.079"),
            scene.getMeshByName("Plan.080"),

            scene.getMeshByName("Plan.081"),
            scene.getMeshByName("Plan.082"),
            scene.getMeshByName("Plan.083"),
            scene.getMeshByName("Plan.084"),
            scene.getMeshByName("Plan.085"),
            scene.getMeshByName("Plan.086"),
            scene.getMeshByName("Plan.087"),
            scene.getMeshByName("Plan.088"),
            scene.getMeshByName("Plan.089"),
            scene.getMeshByName("Plan.090"),

            scene.getMeshByName("Plan.091"),
            scene.getMeshByName("Plan.092"),
            scene.getMeshByName("Plan.093"),
            scene.getMeshByName("Plan.094"),
            scene.getMeshByName("Plan.095"),
            scene.getMeshByName("Plan.096"),
            scene.getMeshByName("Plan.097"),
            scene.getMeshByName("Plan.098"),
            scene.getMeshByName("Plan.099"),
            scene.getMeshByName("Plan.100")

        ];
        personArray = [
            scene.getMeshByName("Plane.001"),
            scene.getMeshByName("Plane.002"),
            scene.getMeshByName("Plane.003"),
            scene.getMeshByName("Plane.004"),
            scene.getMeshByName("Plane.005"),
            scene.getMeshByName("Plane.006"),
            scene.getMeshByName("Plane.007"),
            scene.getMeshByName("Plane.008"),
            scene.getMeshByName("Plane.009"),
            scene.getMeshByName("Plane.010"),

            scene.getMeshByName("Plane.011"),
            scene.getMeshByName("Plane.012"),
            scene.getMeshByName("Plane.013"),
            scene.getMeshByName("Plane.014"),
            scene.getMeshByName("Plane.015"),
            scene.getMeshByName("Plane.016"),
            scene.getMeshByName("Plane.017"),
            scene.getMeshByName("Plane.018"),
            scene.getMeshByName("Plane.019"),
            scene.getMeshByName("Plane.020"),

            scene.getMeshByName("Plane.021"),
            scene.getMeshByName("Plane.022"),
            scene.getMeshByName("Plane.023"),
            scene.getMeshByName("Plane.024"),
            scene.getMeshByName("Plane.025"),
            scene.getMeshByName("Plane.026"),
            scene.getMeshByName("Plane.027"),
            scene.getMeshByName("Plane.028"),
            scene.getMeshByName("Plane.029"),
            scene.getMeshByName("Plane.030"),

            scene.getMeshByName("Plane.031"),
            scene.getMeshByName("Plane.032"),
            scene.getMeshByName("Plane.033"),
            scene.getMeshByName("Plane.034"),
            scene.getMeshByName("Plane.035"),
            scene.getMeshByName("Plane.036"),
            scene.getMeshByName("Plane.037"),
            scene.getMeshByName("Plane.038"),
            scene.getMeshByName("Plane.039"),
            scene.getMeshByName("Plane.040"),

            scene.getMeshByName("Plane.041"),
            scene.getMeshByName("Plane.042"),
            scene.getMeshByName("Plane.043"),
            scene.getMeshByName("Plane.044"),
            scene.getMeshByName("Plane.045"),
            scene.getMeshByName("Plane.046"),
            scene.getMeshByName("Plane.047"),
            scene.getMeshByName("Plane.048"),
            scene.getMeshByName("Plane.049"),
            scene.getMeshByName("Plane.050"),

            scene.getMeshByName("Plane.051"),
            scene.getMeshByName("Plane.052"),
            scene.getMeshByName("Plane.053"),
            scene.getMeshByName("Plane.054"),
            scene.getMeshByName("Plane.055"),
            scene.getMeshByName("Plane.056"),
            scene.getMeshByName("Plane.057"),
            scene.getMeshByName("Plane.058"),
            scene.getMeshByName("Plane.059"),
            scene.getMeshByName("Plane.060"),

            scene.getMeshByName("Plane.061"),
            scene.getMeshByName("Plane.062"),
            scene.getMeshByName("Plane.063"),
            scene.getMeshByName("Plane.064"),
            scene.getMeshByName("Plane.065"),
            scene.getMeshByName("Plane.066"),
            scene.getMeshByName("Plane.067"),
            scene.getMeshByName("Plane.068"),
            scene.getMeshByName("Plane.069"),
            scene.getMeshByName("Plane.070"),

            scene.getMeshByName("Plane.071"),
            scene.getMeshByName("Plane.072"),
            scene.getMeshByName("Plane.073"),
            scene.getMeshByName("Plane.074"),
            scene.getMeshByName("Plane.075"),
            scene.getMeshByName("Plane.076"),
            scene.getMeshByName("Plane.077"),
            scene.getMeshByName("Plane.078"),
            scene.getMeshByName("Plane.079"),
            scene.getMeshByName("Plane.080"),

            scene.getMeshByName("Plane.081"),
            scene.getMeshByName("Plane.082"),
            scene.getMeshByName("Plane.083"),
            scene.getMeshByName("Plane.084"),
            scene.getMeshByName("Plane.085"),
            scene.getMeshByName("Plane.086"),
            scene.getMeshByName("Plane.087"),
            scene.getMeshByName("Plane.088"),
            scene.getMeshByName("Plane.089"),
            scene.getMeshByName("Plane.090"),

            scene.getMeshByName("Plane.091"),
            scene.getMeshByName("Plane.092"),
            scene.getMeshByName("Plane.093"),
            scene.getMeshByName("Plane.094"),
            scene.getMeshByName("Plane.095"),
            scene.getMeshByName("Plane.096"),
            scene.getMeshByName("Plane.097"),
            scene.getMeshByName("Plane.098"),
            scene.getMeshByName("Plane.099"),
            scene.getMeshByName("Plane.100")
        ];
    }
    scene.clearColor = new BABYLON.Color4.FromHexString("#222222FF"); //this is the background colour. Dont forget opacity.
    return scene;
}
function resetForNextQuestion() {
    for (i = 0; i < 100; i++){
        person2Array[i].position.y = 0.5;
        person2Array[i].material = defaultValuePerson;


    }
    answerIsAcquired = false;
}

function theAnswerIs(hereItIs) {
    answerIsAcquired = true;
    if (answerSlider == hereItIs) {

    }
    if (parseInt(answerSlider, 10) > parseInt(hereItIs, 10)) {
        for (var i = 0; i < answerSlider; i++) {
            person2Array[i].material = wrongValuePerson;
        }
        for (var i = 0; i < hereItIs; i++) {
            person2Array[i].material = correctValuePerson;
        }
        /*
        for (var i = answerSlider; i < 100; i++ ){
            person2Array[i].isVisible = false;
        }
        */
    } else {
        for (var i = 0; i < hereItIs; i++) {
            person2Array[i].material = correctValuePerson;
        }
        for (var i = 0; i < answerSlider; i++) {
            person2Array[i].material = wrongValuePerson;
        }
        /*
        for (var i = hereItIs; i < 100; i++ ){
            person2Array[i].isVisible = false;
        }
        */
    }
}

var sceneMaster = createScene();
oldValue = 0;
engine.runRenderLoop(function() {
    if (!answerIsAcquired) {
        if (oldValue <= answerSlider) {
            for (i = 0; i < answerSlider; i++) {
                person2Array[i].material = personTextureMat;
            }

        } else {
            if (oldValue > answerSlider) {
                for (i = 99; i >= answerSlider; i--) {
                    person2Array[i].material = defaultValuePerson;
                }
            }
        }
    }
    oldValue = answerSlider;
    sceneMaster.render();
});


