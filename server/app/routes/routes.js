
var ObjectID = require('mongodb').ObjectID;
var path = require('path');

module.exports = function(app, db) {

    app.get('/', function(req, res) {
        res.sendFile(path.join(__dirname + '../../../../index.html'));
    });

    app.all('/', function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      res.header('Access-Control-Allow-Methods', 'POST');
      res.header('Access-Control-Max-Age', '1000');
      next();
     });

    app.get('/question/:id', (req, res) => {
        //const details = {q_ID: parseInt(req.params.id, 10)};
        const details = {q_ID: req.params.id};
        db.collection('questions').findOne(details, (err, item) => {
            if (err) {
                res.send({ 'error': 'Something has gone wrong!'});
            } else {
                res.jsonp({'res': item});
            }
        });
    });

    // add a question with a new ID
    // populate question pool
    app.post('/question', (req, res) => {
        const q_ID = {
            q_ID: req.body.id,
            question: req.body.question,
            answer: req.body.answer,
            replies: []
        };
        db.collection('questions').insert(q_ID, (err, result) => {
            if (err) {
                res.send({'error': "error happened" + err});
            } else {
                res.send(result.ops[0]);
            }
        });
    });

    // add new replies to questions
    app.post('/answer', (req, res) => {
        const data = {
            answer: req.body.answer,
            q_ID: req.body.questionID
        };
        db.collection('questions').findOneAndUpdate({q_ID: data.q_ID}, {$push: {replies: data.answer}}, (err, result) => {
            if (err) {
                res.send({ 'error': 'Something has gone wrong!'});
            } else {
                res.send(result);
            }
        });
    });

};
