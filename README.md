# 100 Estonians

[www.100estonians.eu](www.100estonians.eu)

Garage48 Visualising Data Hackathon 09-10.05.2019

## Setting up a local development environment

1. Set up and seed the MongoDB database with the contents of `questions.json`.
	- The following command can be used for seeding the database:

		```bash
		mongoimport --host <host>:<port> --username <user> --password <password> --db <dbName> --collection questions --file questions.json --jsonArray --authenticationDatabase admin --type json --ssl
		```

	- Create a `.env` in the project root directory and store the database URL including username and password in the file like so:

		```bash
		DB_URL=<mongodb connection string v2.2.12>
		```

2. Install dependencies

	```bash
	npm install
	```

3. Build Sass styles into CSS

	```bash
	sass sass/style.scss css/style.css --watch
	```

4. Run server

	```bash
	nodemon server.js
	```

## Hosting services

- [Heroku](https://heroku.com) is used for hosting the frontend and API backend. Deployment is performed by pushing to a remote source code repository on the server.
	- More details about the deployment workflow: [Heroku, Deployment with Git](https://devcenter.heroku.com/articles/git).

- [MongoDB Cloud](https://cloud.mongodb.com) was used for hosting the MongoDB database.

## Environment variables

The environment variables are stored in `.env.` in the project root directory.

Contents of `.env`:

```bash
DB_URL="" 	# mongodb connection string v2.2.12
SERVER_PORT="" 	# web server listen port
```
