const express        = require('express');
const MongoClient    = require('mongodb').MongoClient;
const bodyParser     = require('body-parser');
require('dotenv').config()

const app            = express();

const port = 8000;
const path = require('path');

app.use(express.static(path.join(__dirname, 'build')));
app.use("/js", express.static(__dirname + '/js'));
app.use("/css", express.static(__dirname + '/css'));
app.use("/main.js", express.static(__dirname + '/main.js'));
app.use("/es.svg", express.static(__dirname + '/es.svg'));
app.use("/results.png", express.static(__dirname + '/results.png'));
app.use("/results.html", express.static(__dirname + '/results.html'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
console.log("Everything loaded!");

MongoClient.connect(process.env.DB_URL, (err, database) => {
  console.log("Connecting to database!");
  if (err) return console.log(err)
  require('./server/app/routes')(app, database);

  app.listen(process.env.SERVER_PORT || port, () => {
    console.log('We are live on ' + port);
  });
})
